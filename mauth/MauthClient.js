
var request = require('superagent');
var Mauth = require('./mauth');

class MauthClient {
  constructor(appUuid, appPrivateKey, options = {}) {
    this.mauth = new Mauth(appUuid, appPrivateKey, options);
    this.options = options;
  }

  get(baseUri, path, query = {}, extraHeaders = {}) {
    var headers = this.mauth.generateMauthHeaders('GET', path);

    return new Promise((resolve, reject) => {
      request
        .get(baseUri + path)
        .set(headers)
        .set(extraHeaders)
        .query(query)
        .accept('application/json')
        .end(this.handleRequest(resolve, reject, 'MauthClient.get'));
    });
  }

  put(baseUri, path, query = {}, body = '', extraHeaders = {}) {
    var headers = this.mauth.generateMauthHeaders('PUT', path, body);

    return new Promise((resolve, reject) => {
      request
        .put(baseUri + path)
        .send(body)
        .set(headers)
        .set(extraHeaders)
        .query(query)
        .set('Content-Type', 'application/json')
        .accept('application/json')
        .end(this.handleRequest(resolve, reject, 'MauthClient.put'));
    });
  }

  post(baseUri, path, query = {}, body = '', extraHeaders = {}) {
    var headers = this.mauth.generateMauthHeaders('POST', path, body);

    return new Promise((resolve, reject) => {
      request
        .post(baseUri + path)
        .send(body)
        .set(headers)
        .set(extraHeaders)
        .query(query)
        .set('Content-Type', 'application/json')
        .accept('application/json')
        .end(this.handleRequest(resolve, reject, 'MauthClient.post'));
    });
  }

  patch(baseUri, path, query = {}, body = '', extraHeaders = {}) {
    var headers = this.mauth.generateMauthHeaders('PATCH', path, body);

    return new Promise((resolve, reject) => {
      request
        .patch(baseUri + path)
        .send(body)
        .set(headers)
        .set(extraHeaders)
        .query(query)
        .set('Content-Type', 'application/json')
        .accept('application/json')
        .end(this.handleRequest(resolve, reject, 'MauthClient.patch'));
    });
  }

  del(baseUri, path, query = {}, extraHeaders = {}) {
    var headers = this.mauth.generateMauthHeaders('DELETE', path);

    return new Promise((resolve, reject) => {
      request
        .del(baseUri + path)
        .set(headers)
        .set(extraHeaders)
        .query(query)
        .accept('application/json')
        .end(this.handleRequest(resolve, reject, 'MauthClient.del'));
    });
  }

  handleRequest(resolve, reject, message = 'Mauth.handleRequest') {
    return (error, response) => {
      if (error) {
        // console.log(message + ", error:", error, error.response);
        reject(error);
      } else {
        this.options.fullResponse ? resolve(response) : resolve(response.body);
      }
    };
  }
}

module.exports = MauthClient
