const MauthClient = require('./MauthClient');

const fs = require('fs');
const path = require('path');
const url = require('url');
const yaml = require('js-yaml');

let config;
let mauth_key;
try {
  mauth_key = fs.readFileSync(path.join('mauth', 'mauth_key'), 'utf8');
} catch (error) {
  console.log(error);
}
try {
  config = yaml.safeLoad(
    fs.readFileSync(path.join('mauth', 'mauth.yml'), 'utf8')
  );
} catch (error) {
  console.log(error);
}
let mauthClient = new MauthClient(config.common.app_uuid, mauth_key, {
  userUuid: 'f265effa-eea1-42ad-b12c-203aeb967987',
});

module.exports = {
  get: (servicePath, physicalPath, query) => {
    return mauthClient.get(servicePath, physicalPath, query);
  },
};
