'use strict';

var url = require('url');
var crypto = require('crypto');

// Algorithm based on: mAuth Resource Contract Overview-v2.pdf
class Mauth {
  static get MWS_AUTHENTICATION_HEADER() {
    return 'x-mws-authentication';
  }

  static get MWS_TIME_HEADER() {
    return 'x-mws-time';
  }

  static get MCC_IMPERSONATE_HEADER() {
    return 'mcc-impersonate';
  }

  constructor(appUuid, appPrivateKey, options = {}) {
    this.appUuid = appUuid;
    this.appPrivateKey = appPrivateKey.replace(/\\n/g, '\n');
    this.options = options;

    this.userUuid = options.userUuid || null;
  }

  mwsTime() {
    return Math.floor(new Date().getTime() / 1000);
  }

  generateStringToEncrypt(verb, path, body, mwsTime, appUuid) {
    if (body === null) {
      body = '';
    }

    if (typeof body == 'object') {
      body = JSON.stringify(body);
    }

    var str = verb + '\n' + path + '\n' + body + '\n' + appUuid + '\n' + mwsTime;

    let hex = crypto
      .createHash('sha512')
      .update(str)
      .digest('hex');

    return hex;
  }

  generateMauthHeaders(verb, path, body = '', mwsTime = null, appUuid = null) {
    if (body === null) {
      body = '';
    }

    if (typeof body == 'object') {
      body = JSON.stringify(body);
    }

    if (path === null) {
      path = '';
    }

    path = url.parse(path).pathname;

    if (mwsTime === null) {
      mwsTime = this.mwsTime();
    }

    if (appUuid === null) {
      appUuid = this.appUuid;
    }

    let hex = this.generateStringToEncrypt(verb, path, body, mwsTime, appUuid);
    let encryptedHex = crypto.privateEncrypt(this.appPrivateKey, Buffer.from(hex));

    let authHeader = 'MWS ' + appUuid + ':' + encryptedHex.toString('base64');

    let headers = {
      [Mauth.MWS_AUTHENTICATION_HEADER]: authHeader,
      [Mauth.MWS_TIME_HEADER]: mwsTime,
    };

    if (this.userUuid) {
      headers[Mauth.MCC_IMPERSONATE_HEADER] = 'com:mdsol:users:' + this.userUuid;
    }

    return headers;
  }
}

module.exports = Mauth;
